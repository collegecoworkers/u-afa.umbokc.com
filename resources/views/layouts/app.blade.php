@php
	use App\User;
@endphp
<!DOCTYPE HTML>
<html ea>
<head>
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" >
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" >	
	<link href="http://cdn.umbokc.com/ea/src/ea.css?v=1.3" rel="stylesheet">

	<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

</head>
<body>
	<div class="header">
		<div class="logo">
			<a href="/" td:n c#f>Онлайн-торги</a>
		</div>
		<div  class="header-top">
			<div class="header-bottom">
				<div class="h_menu4">
					<a class="toggleMenu" href="#">Menu</a>
					<ul class="nav">
						<li><a href="/">Главная</a></li>
						@auth
							<li><a href="{{ route('/account') }}" >Личный кабинет</a></li>
							<li><a href="{{ route('/my-products') }}" >Покупки</a></li>
							@if (User::isAdmin())
								<li><a href="{{ route('/users') }}" >Пользователи</a></li>
							@endif
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
							<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a></li>
						@endauth
						@guest
							<li><a href="{{ route('login') }}" >Вход</a></li>
						@endguest
					</ul>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>

	@yield('content')

	<div ta:c m:big>
		<p>&copy; {{ config('app.name', 'Laravel') }}. Все права защищены.</p>
	</div>
</body>
</html>

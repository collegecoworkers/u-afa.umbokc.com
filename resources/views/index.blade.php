@extends('layouts.app')
@section('content')
<div m:t:big>
	<div class="col-md-12" ta:c>
		<h4 p:b>Категории</h4>
		@foreach ($cats as $item)
			<a class="btn btn-primary btn-lg" href="{{ route('/cat/{id}', ['id' => $item->id]) }}">{{ $item->title }}</a>
		@endforeach
	</div>
	<div class="clearfix"> </div>
</div>
<div class="product-grids">
	<div class="container">
		<div class="product-top">
			@foreach ($products as $item)
			@php
				$link = route('/product/view/{id}', ['id' => $item->id]);
			@endphp
				<div class="col-md-4 grid-product-in">
					<div class=" product-grid">	
						<a href="{{ $link }}">
							<img width="500" height="300" class="img-responsive " src="{{ $item->getImage() }}" alt="">
						</a>
						<div class="shoe-in">
							<h6><a href="{{ $link }}">{{ $item->title }}</a></h6>
							<label>${{ $item->curr_price }}</label>
							<a href="{{ $link }}" class="store">Подробнее</a>
						</div>
						<b class="plus-on">+</b>
					</div>	
				</div>
			@endforeach
			<div class="clearfix"> </div>
		</div>	
	</div>
</div>
@endsection

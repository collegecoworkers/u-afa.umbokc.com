@php
	use App\Bid;
@endphp
@extends('layouts.app')
@section('content')
<div class="product-grids">
	<div class="container">
		<h2 ta:l>Личный кабинет: мои покупки</h2>
		<div class="product-top">
			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Название</th>
						<th scope="col">Цена</th>
						<th scope="col">Категория</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($products as $item)
						<tr>
							<td>{{$item->id}}</td>
							<td>{{$item->title}}</td>
							<td>${{$item->curr_price}}</td>
							<td>{{$item->getCat()->title}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<div class="clearfix"> </div>
		</div>	
	</div>
</div>
@endsection

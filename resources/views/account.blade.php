@php
	use App\Bid;
@endphp
@extends('layouts.app')
@section('content')
<div class="product-grids">
	<div class="container">
		<h2 ta:l>Личный кабинет: мои продукты</h2>
		<div class="product-top">
			<a href="{{ route('/product/add') }}" class="btn btn-primary">Добавить</a>
			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Название</th>
						<th scope="col">Статус</th>
						<th scope="col">Текущая цена</th>
						<th scope="col">Категория</th>
						<th scope="col">Заявки</th>
						<th scope="col">Действия</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($products as $item)
						<tr>
							<td>{{$item->id}}</td>
							<td>{{$item->title}}</td>
							<td>{{$item->getStatus()}}</td>
							<td>${{$item->curr_price}}</td>
							<td>{{$item->getCat()->title}}</td>
							<td>
								@if ($item->status != 'done')
									<a href="{{ route('/bids/{id}', ['id' => $item->id]) }}">
										{{ Bid::where('product_id', $item->id)->count() }}
										<i class="fa fa-eye"></i>
									</a>
								@else
									<p>{{ Bid::where('product_id', $item->id)->count() }}</p>
								@endif
							</td>
							<td>
								<a href="{{ route('/product/edit/{id}', ['id'=>$item->id]) }}">
									<i class="fa fa-pencil"></i>
								</a>
								<a href="{{ route('/product/edit-img/{id}', ['id'=>$item->id]) }}">
									<i class="fa fa-image"></i>
								</a>
								<a href="{{ route('/product/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
									<i class="fa fa-trash"></i>
								</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<div class="clearfix"> </div>
		</div>	
	</div>
</div>
@endsection

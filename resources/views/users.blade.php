@php
	use App\Bid;
@endphp
@extends('layouts.app')
@section('content')
<div class="product-grids">
	<div class="container">
		<h2 ta:l>Личный кабинет: список пользователей</h2>
		<div class="product-top">
			<a href="{{ route('/user/add') }}" class="btn btn-primary">Добавить</a>
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Имя</th>
                  <th>Логин</th>
                  <th>Email</th>
                  <th>Статус</th>
                  <th>Действия</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($users as $item)
                <tr class="gradeX">
                  <td>{{$item->id}}</td>
                  <td>{{$item->full_name}}</td>
                  <td>{{$item->name}}</td>
                  <td>{{$item->email}}</td>
                  <td>{{$item->getRole()}}</td>
                  <td>
                    <a href="{{ route('/user/edit/{id}', ['id'=>$item->id]) }}">
                      <i class="fa fa-pencil"></i>
                    </a>
                    <a href="{{ route('/user/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
                      <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
			<div class="clearfix"> </div>
		</div>	
	</div>
</div>
@endsection

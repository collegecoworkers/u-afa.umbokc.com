@extends('../layouts.app')
@section('content')
<div class="product-grids">
	<div class="container">
		<h2>Изменить товар</h2>
		@include('product._form')
	</div>
</div>
@endsection

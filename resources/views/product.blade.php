@extends('layouts.app')
@section('content')
<link rel="stylesheet" href="/assets/css/etalage.css">
<script src="/assets/js/jquery.etalage.min.js"></script>
<script>
jQuery(document).ready(function($){
	$('#etalage').etalage({
		thumb_image_width: 300,
		thumb_image_height: 400,
		source_image_width: 900,
		source_image_height: 1200,
		show_hint: true,
		click_callback: function(image_anchor, instance_id){
			alert('Callback example:\nYou clicked on an image with the anchor: "'+image_anchor+'"\n(in Etalage instance: "'+instance_id+'")');
		}
	});
});
</script>
<div class="container">
	<div class="single">
		<div class="col-md-9 top-in-single">
			<div class="col-md-5 single-top">	
				<ul id="etalage" style="">
					<li>
						<a href="optionallink.html">
							<img class="etalage_thumb_image img-responsive" src="{{ $product->getImage() }}" alt="" >
							<img class="etalage_source_image img-responsive" src="{{ $product->getImage() }}" alt="" >
						</a>
					</li>
				</ul>

			</div>	
			<div class="col-md-7 single-top-in">
				<div class="single-para">
					<h4>{{ $product->title }}</h4>
					{!! $product->desc !!}
					<label  class="add-to">${{ $product->curr_price }}</label>
					@if ($product->status != 'done')
						@if ($bid != null)
							<br>
							<p>Ваша цена: <b>${{ $bid->price }}</b></p>
						@endif
						@auth
							<form class="product-top" action="/product/bid/{{ $product->id }}">
								<div>
									<span>Ваша цена</span>
									<br>
									<input type="text" class="form-control" name="price" value="{{ $product->curr_price + 10 }}">
								</div>
								<br>
								<input type="submit" class="btn btn-success" value="Отправить">
								<br>
							</form>
						@endauth
					@else
						<p>Проданно</p>
					@endif
				</div>
			</div>
			<div class="clearfix"> </div>
			<div class="product-top">
				@foreach ($recent as $item)
					@continue($item->id == $product->id)
					@php
					$link = route('/product/view/{id}', ['id' => $item->id]);
					@endphp
					<div class="col-md-4 grid-product-in">	
						<div class=" product-grid">	
							<a href="{{ $link }}">
								<img width="500" height="300" class="img-responsive " src="{{ $item->getImage() }}" alt="">
							</a>
							<div class="shoe-in">
								<h6><a href="{{ $link }}">{{ $item->title }}</a></h6>
								<label>${{ $item->curr_price }}</label>
								<a href="{{ $link }}" class="store">Подробнее</a>
							</div>
							<b class="plus-on">+</b>
						</div>	
					</div>
				@endforeach
				<div class="clearfix"> </div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="single-bottom">
				<h4>Категории</h4>
				<ul>
					@foreach ($cats as $item)
						<li><a href="{{ route('/cat/{id}', ['id' => $item->id]) }}">{{ $item->title }}</a></li>
					@endforeach
				</ul>
			</div>
		</div>
		<div class="clearfix"> </div>		
	</div>
</div>

@endsection

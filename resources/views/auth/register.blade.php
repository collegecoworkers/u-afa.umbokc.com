@extends('../layouts.app')
@section('content')
<div class="container">
	<div class="register">
		<h2 ta:l>Регистрация</h2>
		<br>
		<div class="register-top">
			<form method="post" action="{{ route('register') }}">
				{{ csrf_field() }}
				<div>
					<span>Полное имя</span>
					<input name="full_name" placeholder="Полное имя" type="text">
					@if ($errors->has('full_name'))<span class="help-block"><strong c#f>{{ $errors->first('full_name') }}</strong></span>@endif
				</div>
				<div>
					<span>Логин</span>
					<input name="name" placeholder="Логин" type="text">
					@if ($errors->has('name'))<span class="help-block"><strong c#f>{{ $errors->first('name') }}</strong></span>@endif
				</div>
				<div>
					<span>Email</span>
					<input name="email" placeholder="Email" type="text">
					@if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span>@endif
				</div>
				<div>
					<span>Пароль</span>
					<input name="password" placeholder="Пароль" type="password">
					@if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span>@endif
				</div>
				<div>
					<span>Пароль еще раз</span>
					<input name="password_confirmation" placeholder="Пароль еще раз" type="password">
					@if ($errors->has('password_confirmation'))<span class="help-block"><strong c#f>{{ $errors->first('password_confirmation') }}</strong></span>@endif
				</div>
				<input type="submit" class="button" value="Отправить" cur:p>
				<a href="{{ route('login') }}">Войти</a>
			</form>
		</div>
	</div>
</div>
@endsection

@extends('../layouts.app')
@section('content')
<div class="container">
	<div class="register">
		<h2 ta:l>Вход</h2>
		<br>
		<div class="register-top">
			<form method="post" action="{{ route('login') }}">
				{{ csrf_field() }}
				<div>
					<span>Email</span>
					<input name="email" placeholder="Email" type="text" class="text" />
					@if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span>@endif
				</div>
				<div>
					<span>Пароль</span>
					<input name="password" placeholder="Пароль" type="password" class="text" />
					@if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span>@endif
				</div>
				<input type="submit" class="button" value="Отправить" cur:p>
				<a href="{{ route('register') }}">Регистрация</a>
			</form>
		</div>
	</div>
</div>
@endsection

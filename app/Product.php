<?php
namespace App;

class Product extends MyModel{

	public function getImage() {
		return ($this->image != '') ?
			'/images/' . $this->image :
			'http://placehold.it/500x300';
	}

	public function saveImage() {
		if(request()->image != '' && isset(request()->image)){
			request()->validate(['image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',]);
			$imageName = time().'.'.request()->image->getClientOriginalExtension();
			request()->image->move(public_path('images'), $imageName);
			$this->image = $imageName;
		}
	}

	public function getCat() {
		return Cat::getBy('id', $this->cat_id);
	}

	public function getStatus() {
		return self::getStatusOf($this->status);
	}

	public static function getStatuss(){
		return [
			'in' => 'В процессе',
			'done' => 'Продано',
		];
	}

	public static function getStatusOf($r){
		$statuses = self::getStatuss();
		if(array_key_exists($r, $statuses)) 
			return $statuses[$r];
		return $statuses['in'];
	}
}

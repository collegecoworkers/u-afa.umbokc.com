<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Cat,
	Bid,
	User,
	Product
};

class SiteController extends Controller
{

	public function __construct(){
		// $this->middleware('auth');
	}

	public function Index() {
		return view('index')->with([
			'user' => User::curr(),
			'products' => Product::all(),
			'cats' => Cat::all(),
		]);
	}

	public function Cat($id) {
		return view('index')->with([
			'user' => User::curr(),
			'products' => Product::getsBy('cat_id', $id),
			'cats' => Cat::all(),
		]);
	}

	public function Product($id) {
		$product = Product::getBy('id', $id);
		$recent = Product::where('cat_id', $product->cat_id)->limit(3)->get();
		$bid = null;
		if (User::curr()) {
			$bid = Bid::getBy(['product_id' => $product->id, 'user_id' => User::curr()->id]);
		}
		return view('product')->with([
			'cats' => Cat::all(),
			'bid' => $bid,
			'recent' => $recent,
			'product' => $product,
			'user' => User::curr(),
		]);
	}
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	Cat,
	Bid,
	User,
	Product
};

class ProductController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Account() {
		$user = User::curr();
		return view('account')->with([
			'user' => $user,
			'products' => Product::getsBy('user_id', $user->id),
		]);
	}
	public function Bids($id) {
		$user = User::curr();
		return view('bids')->with([
			'user' => $user,
			'bids' => Bid::getsBy('product_id', $id),
		]);
	}
	public function Bid($id) {
		$price = intval($_GET['price']);

		$p = Product::getById($id);
		$b = Bid::getBy(['product_id' => $p->id, 'user_id' => User::curr()->id]);

		if($b == null){
			$b = new Bid();
			$b->product_id = $p->id;
			$b->user_id = User::curr()->id;
		}

		$b->price = $price;
		$p->curr_price = $price;

		$p->save();
		$b->save();

		return redirect()->back();
	}
	public function Check($id) {

		$bid = Bid::getBy(['id' => $id]);
		$product = Product::getById($bid->product_id);
		$product->status = 'done';
		$product->buyer = $bid->user_id;
	
		$product->save();

		return redirect()->to('/product/view/'.$product->id);
	}
	public function MyProducts() {
		$user = User::curr();
		$products = Product::getsBy(['status'=> 'done', 'buyer' => $user->id]);
		return view('my-products')->with([
			'user' => $user,
			'products' => $products,
		]);
	}

	public function Add() {
		return view('product.add')->with([
			'user' => User::curr(),
			'cats' => Cat::allArr(),
		]);
	}
	public function Edit($id) {
		$model = Product::getBy('id', $id);
		return view('product.edit')->with([
			'model' => $model,
			'cats' => Cat::allArr(),
		]);
	}
	public function EditImg($id) {
		$model = Product::getBy('id', $id);
		return view('product.edit-img')->with([
			'model' => $model,
		]);
	}
	public function Delete($id) {
		Product::where('id', $id)->delete();
		return redirect()->to('/');
	}
	public function Create(Request $request) {
		$model = new Product();

		$model->title = request()->title;
		$model->curr_price = request()->curr_price;
		$model->desc = request()->desc;
		$model->cat_id = request()->cat_id;
		$model->user_id = User::curr()->id;
		$model->buyer = 0;

		$model->save();
		return redirect()->to('/product/edit-img/' . $model->id);
	}
	public function Update($id, Request $request) {
		$model = Product::getBy('id', $id);

		$model->title = request()->title;
		$model->curr_price = request()->curr_price;
		$model->desc = request()->desc;
		$model->cat_id = request()->cat_id;

		$model->save();
		return redirect()->to('/product/view/'.$model->id);
	}
	public function UpdateImg($id, Request $request) {
		$model = Product::getBy('id', $id);
		$model->saveImage();
		$model->save();
		return redirect()->to('/product/view/'.$model->id);
	}
}

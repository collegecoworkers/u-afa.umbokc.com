<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'SiteController@Index')->name('/');
Route::get('/cat/{id}', 'SiteController@Cat')->name('/cat/{id}');

Route::get('/product/view/{id}', 'SiteController@Product')->name('/product/view/{id}');

Route::get('/account', 'ProductController@Account')->name('/account');
Route::get('/my-products', 'ProductController@MyProducts')->name('/my-products');
Route::get('/bids/{id}', 'ProductController@Bids')->name('/bids/{id}');

Route::get('/product/add', 'ProductController@Add')->name('/product/add');
Route::get('/product/edit/{id}', 'ProductController@Edit')->name('/product/edit/{id}');
Route::get('/product/edit-img/{id}', 'ProductController@EditImg')->name('/product/edit-img/{id}');
Route::get('/product/delete/{id}', 'ProductController@Delete')->name('/product/delete/{id}');
Route::get('/product/check/{id}', 'ProductController@Check')->name('/product/check/{id}');
Route::get('/product/bid/{id}', 'ProductController@Bid')->name('/product/bid/{id}');
Route::post('/product/create', 'ProductController@Create')->name('/product/create');
Route::post('/product/update/{id}', 'ProductController@Update')->name('/product/update/{id}');
Route::post('/product/update-img/{id}', 'ProductController@UpdateImg')->name('/product/update-img/{id}');
Route::post('/product/bid/{id}', 'ProductController@Bid')->name('/product/bid/{id}');

Route::get('/users', 'UserController@Users')->name('/users');
Route::get('/user/add', 'UserController@Add')->name('/user/add');
Route::get('/user/edit/{id}', 'UserController@Edit')->name('/user/edit/{id}');
Route::get('/user/delete/{id}', 'UserController@Delete')->name('/user/delete/{id}');
Route::post('/user/create', 'UserController@Create')->name('/user/create');
Route::post('/user/update/{id}', 'UserController@Update')->name('/user/update/{id}');
